"use strict";

const del = require("del");
const gulp = require("gulp");
const gulpSass = require("gulp-sass");
const gulpPug = require("gulp-pug");
const gulpAutoprefixer = require("gulp-autoprefixer");
const gulpPlumber = require("gulp-plumber");
const gulpImagemin = require("gulp-imagemin");
const gulpNewer = require("gulp-newer");
const imageminPngquant = require("imagemin-pngquant");

const paths = {
  dist: {
    html: "dist/",
    css: "dist/assets/css/",
    js: "dist/assets/js/",
    images: "dist/assets/images/",
    fonts: "dist/assets/fonts/"
  },
  src: {
    html: "src/pug/*.pug",
    css: "src/scss/style.scss",
    js: "src/js/*.js",
    images: "src/images/**/*.*",
    fonts: "src/fonts/**/*.*"
  },
  watch: {
    html: "src/pug/**/*.pug",
    css: "src/scss/**/*.scss",
    js: "src/js/**/*.js",
    images: "src/images/**/*.*",
    fonts: "src/fonts/**/*.*"
  }
};

const clean = () => del(paths.dist.html);
const cleanHtml = () => del(paths.dist.html + "*.html");
const cleanCss = () => del(paths.dist.css);
const cleanFonts = () => del(paths.dist.fonts);

const sass = () =>
  gulp
    .src(paths.src.css)
    .pipe(gulpPlumber())
    .pipe(
      gulpSass({
        outputStyle: "compressed"
      })
    )
    .pipe(
      gulpAutoprefixer({
        browsers: [">1%"],
        cascade: false
      })
    )
    .pipe(gulp.dest(paths.dist.css));

const pug = () =>
  gulp
    .src(paths.src.html)
    .pipe(gulpPlumber())
    .pipe(
      gulpPug({
        cache: true
      })
    )
    .pipe(gulp.dest(paths.dist.html));

const images = () =>
  gulp
    .src(paths.src.images, { allowEmpty: true })
    .pipe(gulpNewer(paths.dist.images))
    .pipe(
      gulpImagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        use: [imageminPngquant()],
        interlaced: true
      })
    )
    .pipe(gulp.dest(paths.dist.images));

const fonts = () =>
  gulp
    .src(paths.src.fonts, { allowEmpty: true })
    .pipe(gulp.dest(paths.dist.fonts));

const watch = () => {
  gulp.watch(paths.watch.html, gulp.series(cleanHtml, pug));
  gulp.watch(paths.watch.css, gulp.series(cleanCss, sass));
  gulp.watch(paths.watch.fonts, gulp.series(cleanFonts, fonts));

  const imagesWatcher = gulp.watch(paths.watch.images, images);
  imagesWatcher.on("unlink", unlinkPath => {
    const filePathFromSrc = path.relative(
      path.resolve("src/images"),
      unlinkPath
    );
    const distFilePath = path.resolve("dist/images", filePathFromSrc);
    del(distFilePath);
    console.log("Delete file: " + distFilePath);
  });
};

gulp.task(
  "default",
  gulp.series(clean, gulp.parallel(pug, sass, images, fonts), watch)
);
